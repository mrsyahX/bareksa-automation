<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Bareksa-Web</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>240e9c05-353c-496f-ac5c-0e599b3d779c</testSuiteGuid>
   <testCaseLink>
      <guid>82ddd1fc-09ea-464f-b361-5ed527101c59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/TC-REK001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1aa43d50-9e4c-487d-a7c6-9a951365fb1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/TC-BER001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d52d3c0-7819-4670-b80e-82796350c805</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/TC-SBN001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4340b51-601a-4c8b-a000-7c5c5bc19bef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/TC-EMA001</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
